module.exports = function(grunt) {
	// Show grunt time
	require('time-grunt')(grunt);
	require('load-grunt-tasks')(grunt);
	
	// Project configuration
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		connect: {
			server: {
				options: {
					port: 8080,
					base: 'online-help',
					open: {
						target: 'http://localhost:8080'
					}
				}
			}
		},

		sass: {
			options: {
					sourceMap: false
				},
			dist: {
				files: {
					'online-help/style/style.css' : 'source/style/style.scss'
				}
			}
		},

		copy: {
			dist: {
				files: [{
					expand: true,
					flatten: true,
					src: ['source/*'],
					dest: 'dist/',
					filter: 'isFile'
				}]
			}
		},

		jade: {
		  compile: {
		    options: {
		      data: {
		        debug: false
		      },
		      pretty: true
		    },
		    files: [{
				cwd: 'source/docs',
				src: '**/*.jade',
				dest: 'online-help',
				expand: true,
				ext: '.html'
			}]
		  }
		},

		watch: {
			options: {
				livereload: 35729
			},
			css: {
				files: ['source/**/*'],
				tasks: ['build-dev'],
			}
		}

	});

	// Load plugins -------------------------------------

	// Watch the repo
	grunt.loadNpmTasks('grunt-contrib-watch');
	// Create a server
	grunt.loadNpmTasks('grunt-contrib-connect');
	// Copy over files from dist to source
	grunt.loadNpmTasks('grunt-contrib-copy');
	// Jade for templates
	grunt.loadNpmTasks('grunt-contrib-jade');

	// Tasks --------------------------------------------

	grunt.registerTask('default', ['build']);
	grunt.registerTask('build', ['jade','copy','sass','connect', 'watch']);
	grunt.registerTask('build-dev',['jade','copy','sass']);

};